package com.codiceplastico.courses.java;

public class Person {
    private static final String Empty = "";
    private final String name;

    public Person(String name) {
        this.name = name;
    }

    public Person() {
        this(Empty);
    }

    public void foo() {
        //this.name = "456";
    }
}
