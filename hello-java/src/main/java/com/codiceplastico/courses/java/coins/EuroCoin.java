package com.codiceplastico.courses.java.coins;

import java.math.BigDecimal;

enum EuroCoin0 {
    Two, One, FiftyCents, TwentyCents, TenCents, FiveCents, TwoCents, OneCent
}

// Compilation error
//enum EuroCoin2 {
//    Two = 2, One = 1, FiftyCents = 0.50
//}

public enum EuroCoin {
    Two(BigDecimal.valueOf(2)), One(BigDecimal.valueOf(2)), FiftyCents(BigDecimal.valueOf(0.50)),
    TwentyCents(BigDecimal.valueOf(0.20)), TenCents(BigDecimal.valueOf(0.10)),
    FiveCents(BigDecimal.valueOf(0.05)),
    TwoCents(BigDecimal.valueOf(0.02)), OneCent(BigDecimal.valueOf(0.01));
    private BigDecimal value;


    EuroCoin(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }
}
