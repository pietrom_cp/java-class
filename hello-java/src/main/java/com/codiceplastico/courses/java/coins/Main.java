package com.codiceplastico.courses.java.coins;

public class Main {
    public static void main(String[] args) {
        printCoin(EuroCoin.Two);
        printCoin(EuroCoin.One);
        printCoin(EuroCoin.FiftyCents);

        System.out.println(EuroCoin.FiftyCents == EuroCoin.FiftyCents);
    }

    private static void printCoin(EuroCoin coin) {
        System.out.println(coin);
        System.out.println(coin.getValue());
    }
}
