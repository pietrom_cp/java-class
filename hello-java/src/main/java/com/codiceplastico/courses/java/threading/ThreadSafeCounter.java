package com.codiceplastico.courses.java.threading;

public class ThreadSafeCounter implements Counter {
    private int count;

    public ThreadSafeCounter() {
        count = 0;
    }

    @Override
    public int getValue() {
        return count;
    }

    @Override
    public synchronized void increment() {
        count++;
    }
}
